import argparse
from glob import glob

ROOT.xAOD.Init()

parser = argparse.ArgumentParser()
parser.add_argument('sample', type=str)
args = vars(parser.parse_args())
sample_path = args['sample']

f_list = glob(sample_path)
t_chain = ROOT.TChain('CollectionTree')
for f in f_list:
  t_chain.AddFile(f)
t = ROOT.xAOD.MakeTransientTree(t_chain)

print("Tester")
print(t.GetEntries())
