from bbtautau_helpers import Sample
import sys
import awkward as ak
import copy

sys.path.append('/eos/user/n/nicholas/SWAN_projects/Derivation/scripts/')
from source_samples import ttbar_source, zhf_source, whf_taunu_source, whf_enu_source, whf_munu_source, sig10_source, sig16_source, sig20_source, jz3w_source, jz4w_source, jz5w_source, jz6w_source, data_source

from sample_naming import SampleNaming, sig10_sample_naming, sig16_sample_naming, sig20_sample_naming, zhf_sample_naming, ttbar_sample_naming, whf_taunu_sample_naming, whf_enu_sample_naming, whf_munu_sample_naming, jz3w_sample_naming, jz4w_sample_naming, jz5w_sample_naming, jz6w_sample_naming, data_sample_naming

# All values in fb/fb^-1
luminosity = 139
signal_xsection = 8.29

naming_kwargs_v0 = SampleNaming.format_kwargs(skim_yn=1, version='V0', flat_yn=0)
naming_kwargs_v1 = SampleNaming.format_kwargs(skim_yn=1, version='V1', flat_yn=0)
naming_kwargs_test0 = SampleNaming.format_kwargs(skim_yn=1, version='TEST0', flat_yn=0)

kwargs_dict = {
    'V0' : naming_kwargs_v0,
    'V1' : naming_kwargs_v1,
    'TEST0' : naming_kwargs_test0
    }

sig10_version = 'V0'
sig10_kwargs = kwargs_dict[sig10_version]
sig10_file_path = sig10_sample_naming.file_path(sig10_kwargs)
sig10_sample = Sample(sig10_file_path, 1)
sig10_sample.create_branches()
sig10_sample.naming = sig10_sample_naming
sig10_sample.did_name = sig10_sample_naming.did_name
sig10_sample.short_name = '1 TeV'
sig10_sample.luminosity = luminosity
sig10_sample.xsection = signal_xsection
sig10_sample.genfiltereff = 0.20006
sig10_sample.source_events = sig10_source.input_events
sig10_sample.is_skimmed = sig10_sample_naming.skim_yn
sig10_sample.use_mcevent_weights = 0
sig10_sample.calculate_mc_weight()
sig10_sample.calculate_event_weights()

sig16_version = 'V0'
sig16_kwargs = kwargs_dict[sig16_version]
sig16_file_path = sig16_sample_naming.file_path(sig16_kwargs)
sig16_sample = Sample(sig16_file_path, 1)
sig16_sample.create_branches()
sig16_sample.naming = sig16_sample_naming
sig16_sample.did_name = sig16_sample_naming.did_name
sig16_sample.short_name = '1.6 TeV'
sig16_sample.luminosity = luminosity
sig16_sample.xsection = signal_xsection
sig16_sample.genfiltereff = 0.20006
sig16_sample.source_events = sig16_source.input_events
sig16_sample.is_skimmed = sig16_sample_naming.skim_yn
sig16_sample.use_mcevent_weights = 0
sig16_sample.calculate_mc_weight()
sig16_sample.calculate_event_weights()

sig20_version = 'V0'
sig20_kwargs = kwargs_dict[sig20_version]
sig20_file_path = sig20_sample_naming.file_path(sig20_kwargs)
sig20_sample = Sample(sig20_file_path, 1)
sig20_sample.create_branches()
sig20_sample.naming = sig20_sample_naming
sig20_sample.did_name = sig20_sample_naming.did_name
sig20_sample.short_name = '2 TeV'
sig20_sample.luminosity = luminosity
sig20_sample.xsection = signal_xsection
sig20_sample.genfiltereff = 0.20697
sig20_sample.source_events = sig20_source.input_events
sig20_sample.is_skimmed = sig20_sample_naming.skim_yn
sig20_sample.use_mcevent_weights = 0
sig20_sample.calculate_mc_weight()
sig20_sample.calculate_event_weights()

ttbar_version = 'V1'
ttbar_kwargs = kwargs_dict[ttbar_version]
ttbar_file_path = ttbar_sample_naming.file_path(ttbar_kwargs)
ttbar_sample = Sample(ttbar_file_path, 0)
ttbar_sample.create_branches()
ttbar_sample.naming = ttbar_sample_naming
ttbar_sample.did_name = ttbar_sample_naming.did_name
ttbar_sample.short_name = 'ttbar'
ttbar_sample.luminosity = luminosity
ttbar_sample.xsection = 730000
ttbar_sample.genfiltereff = 0.54385
ttbar_sample.source_events = ttbar_source.input_events[ttbar_version]
ttbar_sample.is_skimmed = ttbar_sample_naming.skim_yn
ttbar_sample.use_mcevent_weights = 0
ttbar_sample.calculate_mc_weight()
ttbar_sample.calculate_event_weights()

zhf_version = 'V1'
zhf_kwargs = kwargs_dict[zhf_version]
zhf_file_path = zhf_sample_naming.file_path(zhf_kwargs)
zhf_sample = Sample(zhf_file_path, 0)
zhf_sample.create_branches()
zhf_sample.naming = zhf_sample_naming
zhf_sample.did_name = zhf_sample_naming.did_name
zhf_sample.short_name = 'Z+jets'
zhf_sample.luminosity = luminosity
zhf_sample.xsection = 8680
zhf_sample.genfiltereff = 0.17623
zhf_sample.source_events = zhf_source.input_events[zhf_version]
zhf_sample.is_skimmed = zhf_sample_naming.skim_yn
zhf_sample.use_mcevent_weights = 0
zhf_sample.calculate_mc_weight()
zhf_sample.calculate_event_weights()

whf_taunu_version = 'TEST0'
whf_taunu_kwargs = kwargs_dict[whf_taunu_version]
whf_taunu_file_path = whf_taunu_sample_naming.file_path(whf_taunu_kwargs)
whf_taunu_sample = Sample(whf_taunu_file_path, 0)
whf_taunu_sample.create_branches()
whf_taunu_sample.naming = whf_taunu_sample_naming
whf_taunu_sample.did_name = whf_taunu_sample_naming.did_name
whf_taunu_sample.short_name = 'W+jets_taunu'
whf_taunu_sample.luminosity = luminosity
whf_taunu_sample.xsection = 71944
whf_taunu_sample.genfiltereff = 0.13597
whf_taunu_sample.source_events = whf_taunu_source.input_events
whf_taunu_sample.is_skimmed = whf_taunu_sample_naming.skim_yn
whf_taunu_sample.use_mcevent_weights = 0
whf_taunu_sample.calculate_mc_weight()
whf_taunu_sample.calculate_event_weights()

whf_enu_version = 'TEST0'
whf_enu_kwargs = kwargs_dict[whf_enu_version]
whf_enu_file_path = whf_enu_sample_naming.file_path(whf_enu_kwargs)
whf_enu_sample = Sample(whf_enu_file_path, 0)
whf_enu_sample.create_branches()
whf_enu_sample.naming = whf_enu_sample_naming
whf_enu_sample.did_name = whf_enu_sample_naming.did_name
whf_enu_sample.short_name = 'W+jets_enu'
whf_enu_sample.luminosity = luminosity
whf_enu_sample.xsection = 72077
whf_enu_sample.genfiltereff = 0.13865
whf_enu_sample.source_events = whf_enu_source.input_events
whf_enu_sample.is_skimmed = whf_enu_sample_naming.skim_yn
whf_enu_sample.use_mcevent_weights = 0
whf_enu_sample.calculate_mc_weight()
whf_enu_sample.calculate_event_weights()

whf_munu_version = 'TEST0'
whf_munu_kwargs = kwargs_dict[whf_munu_version]
whf_munu_file_path = whf_munu_sample_naming.file_path(whf_munu_kwargs)
whf_munu_sample = Sample(whf_munu_file_path, 0)
whf_munu_sample.create_branches()
whf_munu_sample.naming = whf_munu_sample_naming
whf_munu_sample.did_name = whf_munu_sample_naming.did_name
whf_munu_sample.short_name = 'W+jets_munu'
whf_munu_sample.luminosity = luminosity
whf_munu_sample.xsection = 72063
whf_munu_sample.genfiltereff = 0.13137
whf_munu_sample.source_events = whf_munu_source.input_events
whf_munu_sample.is_skimmed = whf_munu_sample_naming.skim_yn
whf_munu_sample.use_mcevent_weights = 0
whf_munu_sample.calculate_mc_weight()
whf_munu_sample.calculate_event_weights()

jz3w_version = 'V2_0'
jz3w_kwargs = SampleNaming.format_kwargs(skim_yn=1, version='V2_0', flat_yn=0)
jz3w_file_path = jz3w_sample_naming.file_path(jz3w_kwargs)
jz3w_sample = Sample(jz3w_file_path, 0)
jz3w_sample.create_branches()
jz3w_sample.naming = jz3w_sample_naming
jz3w_sample.did_name = jz3w_sample_naming.did_name
jz3w_sample.short_name = 'JZ3W'
jz3w_sample.luminosity = luminosity
jz3w_sample.xsection = 26500000000
jz3w_sample.genfiltereff = 0.00032012
jz3w_sample.source_events = jz3w_source.input_events
jz3w_sample.is_skimmed = jz3w_sample_naming.skim_yn
jz3w_sample.use_mcevent_weights = 1
jz3w_sample.calculate_mc_weight()
jz3w_sample.calculate_event_weights()

jz4w_version = 'V2_0'
jz4w_kwargs = SampleNaming.format_kwargs(skim_yn=1, version='V2_0', flat_yn=0)
jz4w_file_path = jz4w_sample_naming.file_path(jz4w_kwargs)
jz4w_sample = Sample(jz4w_file_path, 0)
jz4w_sample.create_branches()
jz4w_sample.naming = jz4w_sample_naming
jz4w_sample.did_name = jz4w_sample_naming.did_name
jz4w_sample.short_name = 'JZ4W'
jz4w_sample.luminosity = luminosity
jz4w_sample.xsection = 254630000
jz4w_sample.genfiltereff = 0.00053137
jz4w_sample.source_events = jz4w_source.input_events
jz4w_sample.is_skimmed = jz4w_sample_naming.skim_yn
jz4w_sample.use_mcevent_weights = 1
jz4w_sample.calculate_mc_weight()
jz4w_sample.calculate_event_weights()

jz5w_version = 'V2_0'
jz5w_kwargs = SampleNaming.format_kwargs(skim_yn=1, version='V2_0', flat_yn=0)
jz5w_file_path = jz5w_sample_naming.file_path(jz5w_kwargs)
jz5w_sample = Sample(jz5w_file_path, 0)
jz5w_sample.create_branches()
jz5w_sample.naming = jz5w_sample_naming
jz5w_sample.did_name = jz5w_sample_naming.did_name
jz5w_sample.short_name = 'JZ5W'
jz5w_sample.luminosity = luminosity
jz5w_sample.xsection = 4553500
jz5w_sample.genfiltereff = 0.00092395
jz5w_sample.source_events = jz5w_source.input_events
jz5w_sample.is_skimmed = jz5w_sample_naming.skim_yn
jz5w_sample.use_mcevent_weights = 1
jz5w_sample.calculate_mc_weight()
jz5w_sample.calculate_event_weights()

jz6w_version = 'V2_0'
jz6w_kwargs = SampleNaming.format_kwargs(skim_yn=1, version='V2_0', flat_yn=0)
jz6w_file_path = jz6w_sample_naming.file_path(jz6w_kwargs)
jz6w_sample = Sample(jz6w_file_path, 0)
jz6w_sample.create_branches()
jz6w_sample.naming = jz6w_sample_naming
jz6w_sample.did_name = jz6w_sample_naming.did_name
jz6w_sample.short_name = 'JZ6W'
jz6w_sample.luminosity = luminosity
jz6w_sample.xsection = 257530
jz6w_sample.genfiltereff = 0.0009427
jz6w_sample.source_events = jz6w_source.input_events
jz6w_sample.is_skimmed = jz6w_sample_naming.skim_yn
jz6w_sample.use_mcevent_weights = 1
jz6w_sample.calculate_mc_weight()
jz6w_sample.calculate_event_weights()

data_version = 'V2_2'
data_kwargs = SampleNaming.format_kwargs(skim_yn=1, version=data_version, flat_yn=0)
data_file_path = data_sample_naming.file_path(data_kwargs)
data_sample = Sample(data_file_path, 0)
data_sample.create_branches()
data_sample.naming = data_sample_naming
data_sample.did_name = data_sample_naming.did_name
data_sample.short_name = 'Data'
data_sample.source_events = data_source.input_events
data_sample.is_skimmed = data_sample_naming.skim_yn
data_sample.use_mcevent_weights = 0
data_sample.calculate_mc_weight()
data_sample.calculate_event_weights()

# Create combined JZW (hacky) sample
#jzw_samples = [jz3w_sample, jz4w_sample, jz5w_sample, jz6w_sample]
#branches_list = []
#weights_list = []
#for i, sample in enumerate(jzw_samples):
#  branches_list.append(sample.branches)
#jzw_branches = ak.concatenate(branches_list)
#jzw_sample = Sample('', 0)
#jzw_sample.branches = jzw_branches
#jzw_sample.eff_branches = jzw_branches
#jzw_sample.short_name = 'JZW'
#jzw_sample.is_composite = 1
#jzw_sample.did_name = 'jzw'

jzw_samples = [jz3w_sample, jz4w_sample, jz5w_sample, jz6w_sample]
jzw_sample = Sample.composite_sample(0, 'JZW', jzw_samples)
jzw_sample.create_branches()
jzw_sample.did_name = 'jzw'

whf_samples = [whf_taunu_sample, whf_enu_sample, whf_munu_sample]
whf_sample = Sample.composite_sample(0, 'W+jets', whf_samples)
whf_sample.create_branches()
whf_sample.did_name = 'whf'

signal_samples = [sig10_sample, sig16_sample, sig20_sample]
background_samples = [ttbar_sample, zhf_sample, whf_sample, jzw_sample]
expanded_background_samples = [ttbar_sample, zhf_sample, whf_taunu_sample, whf_enu_sample, whf_munu_sample, jz3w_sample, jz4w_sample, jz5w_sample, jz6w_sample]
sample_dict = { 'X1000' : sig10_sample,
                'X1600' : sig16_sample,
                'X2000' : sig20_sample,
                'Z+jets' : zhf_sample,
                'ttbar' : ttbar_sample ,
                'W+jets_taunu' : whf_taunu_sample ,
                'W+jets_enu' : whf_enu_sample ,
                'W+jets_munu' : whf_munu_sample ,
                'W+jets' : whf_sample ,
                'JZ3W' : jz3w_sample ,
                'JZ4W' : jz4w_sample ,
                'JZ5W' : jz5w_sample ,
                'JZ6W' : jz6w_sample ,
                'JZW'  : jzw_sample ,
                }
all_samples = list(sample_dict.values())
