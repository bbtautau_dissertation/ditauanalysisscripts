class BranchNames:
    def __init__(self):
        self.input_branch_names = ['LeptonPt',
                                'LeptonEta',
                                'LeptonPhi',
                                'LeptonE',
                                'TauPt',
                                'TauEta',
                                'TauPhi',
                                'TauE', 
                                'LepTauPt',
                                'LepTauEta',
                                'LepTauPhi',
                                'LepTauE',
                                'LepTauM',
                                'LargeRJetPt',
                                'LargeRJetEta',
                                'LargeRJetPhi',
                                'LargeRJetE',
                                'LargeRJetM',
                                'IsElectron'
                                ]
        self.label_branch_name = 'IsSignal'
  
        self.extra_branch_names = ['UniqueEventID',
                             'SampleID',
                             'LeptonTruthdR',
                             'TauTruthdR',
                             'LargeRJetTruthdR',
                             'DiHiggsM'] + self.input_branch_names
        
        self.output_branch_name = 'Pred'
        self.tiebreaker_branch_name = 'IsElectron'
        
        self.id_branch_name = 'UniqueEventID'

branch_names = BranchNames()
