import uproot
from sample_naming import SampleNaming, sig10_sample_naming, sig16_sample_naming, sig20_sample_naming, ttbar_sample_naming, zhf_sample_naming, jz3w_sample_naming, jz4w_sample_naming, jz5w_sample_naming, jz6w_sample_naming
from branch_names import branch_names

even_cut_str = 'EventNumber%2==0'
odd_cut_str = 'EventNumber%2==1'

class FlatSample:
    def __init__(self, f_path, cut=''):
        self.f_path = f_path
        self.file = uproot.open(self.f_path)
        self.tree = self.file['mytree']
        if cut:
          self.branches = self.tree.arrays(cut=cut)
        else:
          self.branches = self.tree.arrays()
  
file_path = '../samples/{}'

naming_kwargs = SampleNaming.format_kwargs(skim_yn=1, version='V0', flat_yn=1)
jzw_kwargs = SampleNaming.format_kwargs(skim_yn=1, version='V2_0', flat_yn=1)

sig10_file_path = sig10_sample_naming.file_path(naming_kwargs)
sig10_sample = FlatSample(sig10_file_path)
sig10_even_sample = FlatSample(sig10_file_path, even_cut_str)
sig10_odd_sample = FlatSample(sig10_file_path, odd_cut_str)

sig16_file_path = sig16_sample_naming.file_path(naming_kwargs)
sig16_sample = FlatSample(sig16_file_path)
sig16_even_sample = FlatSample(sig16_file_path, even_cut_str)
sig16_odd_sample = FlatSample(sig16_file_path, odd_cut_str)

sig20_file_path = sig20_sample_naming.file_path(naming_kwargs)
sig20_sample = FlatSample(sig20_file_path)
sig20_even_sample = FlatSample(sig20_file_path, even_cut_str)
sig20_odd_sample = FlatSample(sig20_file_path, odd_cut_str)

ttbar_file_path = ttbar_sample_naming.file_path(naming_kwargs)
ttbar_sample = FlatSample(ttbar_file_path)
ttbar_even_sample = FlatSample(ttbar_file_path, even_cut_str)
ttbar_odd_sample = FlatSample(ttbar_file_path, odd_cut_str)

zhf_file_path = zhf_sample_naming.file_path(naming_kwargs)
zhf_sample = FlatSample(zhf_file_path)
zhf_even_sample = FlatSample(zhf_file_path, even_cut_str)
zhf_odd_sample = FlatSample(zhf_file_path, odd_cut_str)

jz3w_file_path = jz3w_sample_naming.file_path(jzw_kwargs)
jz3w_sample = FlatSample(jz3w_file_path)
jz3w_even_sample = FlatSample(jz3w_file_path, even_cut_str)
jz3w_odd_sample = FlatSample(jz3w_file_path, odd_cut_str)

jz4w_file_path = jz4w_sample_naming.file_path(jzw_kwargs)
jz4w_sample = FlatSample(jz4w_file_path)
jz4w_even_sample = FlatSample(jz4w_file_path, even_cut_str)
jz4w_odd_sample = FlatSample(jz4w_file_path, odd_cut_str)

jz5w_file_path = jz5w_sample_naming.file_path(jzw_kwargs)
jz5w_sample = FlatSample(jz5w_file_path)
jz5w_even_sample = FlatSample(jz5w_file_path, even_cut_str)
jz5w_odd_sample = FlatSample(jz5w_file_path, odd_cut_str)

jz6w_file_path = jz6w_sample_naming.file_path(jzw_kwargs)
jz6w_sample = FlatSample(jz6w_file_path)
jz6w_even_sample = FlatSample(jz6w_file_path, even_cut_str)
jz6w_odd_sample = FlatSample(jz6w_file_path, odd_cut_str)

samples = { 'X1000' : sig10_sample,
            'X1600' : sig16_sample,
            'X2000' : sig20_sample,
            'ttbar' : ttbar_sample,
            'Z+jets' : zhf_sample,
            'JZ3W' : jz3w_sample,
            'JZ4W' : jz4w_sample,
            'JZ5W' : jz5w_sample,
            'JZ6W' : jz6w_sample,
            }

even_samples = { 'X1000' : sig10_even_sample,
            'X1600' : sig16_even_sample,
            'X2000' : sig20_even_sample,
            'ttbar' : ttbar_even_sample,
            'Z+jets' : zhf_even_sample, 
            'JZ3W' : jz3w_even_sample,
            'JZ4W' : jz4w_even_sample,
            'JZ5W' : jz5w_even_sample,
            'JZ6W' : jz6w_even_sample,
            }

odd_samples = { 'X1000' : sig10_odd_sample,
            'X1600' : sig16_odd_sample,
            'X2000' : sig20_odd_sample,
            'ttbar' : ttbar_odd_sample,
            'Z+jets' : zhf_odd_sample, 
            'JZ3W' : jz3w_odd_sample,
            'JZ4W' : jz4w_odd_sample,
            'JZ5W' : jz5w_odd_sample,
            'JZ6W' : jz6w_odd_sample,
            }

sample_names = ['X1000', 'X1600', 'X2000', 'ttbar', 'Z+jets', 'JZ3W', 'JZ4W', 'JZ5W', 'JZ6W']
signal_sample_names = ['X1000', 'X1600', 'X2000']
background_sample_names = ['ttbar', 'Z+jets', 'JZ3W', 'JZ4W', 'JZ5W', 'JZ6W']
sample_id_dict = {}
sample_id = 0
# Setup sample dictionary and fill sample IDs
for name in samples.keys():
  sample_id_dict[name] = sample_id
  samples[name].branches['SampleID'] = sample_id
  even_samples[name].branches['SampleID'] = sample_id
  odd_samples[name].branches['SampleID'] = sample_id
  sample_id += 1
