from bbtautau_helpers import Sample

file_path_template = '../samples/{}.ntuple.root' 
# All values in fb/fb^-1
luminosity = 139
signal_xsection = 8.29

sig10_sample = Sample(file_path_template.format('450522_X1000_bbtautau_lephad'), 1)
sig10_sample.short_name = '1 TeV'
sig10_sample.luminosity = luminosity
sig10_sample.xsection = signal_xsection
sig10_sample.genfiltereff = 0.20006
sig10_sample.create_branches()
sig10_sample.calculate_mc_weight()

sig16_sample = Sample(file_path_template.format('450166_X1600_bbtautau_lephad'), 1)
sig16_sample.short_name = '1.6 TeV'
sig16_sample.luminosity = luminosity
sig16_sample.xsection = signal_xsection
sig16_sample.genfiltereff = 0.20006
sig16_sample.create_branches()
sig16_sample.calculate_mc_weight()

sig20_sample = Sample(file_path_template.format('450524_X2000_bbtautau_lephad'), 1)
sig20_sample.short_name = '2 TeV'
sig20_sample.luminosity = luminosity
sig20_sample.xsection = signal_xsection
sig20_sample.genfiltereff = 0.20697
sig20_sample.create_branches()
sig20_sample.calculate_mc_weight()

ttbar_sample = Sample(file_path_template.format('410470_ttbar_hdamp258p75_nonallhad'), 0)
ttbar_sample.short_name = 'ttbar'
ttbar_sample.luminosity = luminosity
ttbar_sample.xsection = 730000
ttbar_sample.genfiltereff = 0.54385
ttbar_sample.create_branches()
ttbar_sample.calculate_mc_weight()

zhf_sample = Sample(file_path_template.format('364139_Ztautau_BFilter'), 0)
zhf_sample.short_name = 'Z+jets'
zhf_sample.luminosity = luminosity
zhf_sample.xsection = 8680
zhf_sample.genfiltereff = 0.17623
zhf_sample.create_branches()
zhf_sample.calculate_mc_weight()
'''
whf_sample = Sample(file_path_template.format('364186_Wtaunu_BFilter'), 0)
whf_sample.short_name = 'W+jets'
whf_sample.luminosity = luminosity
whf_sample.xsection = 19200000
whf_sample.genfiltereff = 0.04513
whf_sample.create_branches()
whf_sample.calculate_mc_weight()
'''
signal_samples = [sig10_sample, sig16_sample, sig20_sample]
background_samples = [ttbar_sample, zhf_sample]
all_samples = signal_samples + background_samples
