import sys

sys.path.append('/eos/user/n/nicholas/SWAN_projects/Derivation/scripts/')
from source_samples import ttbar_source, zhf_source, whf_taunu_source, whf_enu_source, whf_munu_source, sig10_source, sig16_source, sig20_source, jz3w_source, jz4w_source, jz5w_source, jz6w_source, data_source

class SampleNaming:
  file_path_template = '../samples/{}'
  ntuple_template = '{did}.DAOD_HIGGBOOSTEDLH{skim}.{version}.{file_format}.root'
  skimmed_fill = { 1 : '', 0 : '_NOSKIM' }
  format_fill = { 0 : 'ntuple' , 1 : 'flat_ntuple' }
  default_version = 'V0'

  @classmethod
  def format_kwargs(cls, skim_yn, version, flat_yn):
    out = {}
    out['skim'] = cls.skimmed_fill[skim_yn]
    out['version'] = version
    out['file_format'] = cls.format_fill[flat_yn]
    return out

  def __init__(self, did_name, skim_yn=1, version=default_version, flat_yn=0):
    self.did_name = did_name
    self.skim_yn = skim_yn
    self.flat_yn = flat_yn
    self.skim = self.skimmed_fill[skim_yn]
    self.version = version
    self.file_format = self.format_fill[flat_yn]
    
  def ntuple_name(self, naming_kwargs):
    return self.ntuple_template.format(did=self.did_name, **naming_kwargs)
    
  def file_path(self, naming_kwargs):
    ntuple_name = self.ntuple_name(naming_kwargs)
    return self.file_path_template.format(ntuple_name)

sig10_sample_naming = SampleNaming(sig10_source.did_name)
sig16_sample_naming = SampleNaming(sig16_source.did_name)
sig20_sample_naming = SampleNaming(sig20_source.did_name)
zhf_sample_naming = SampleNaming(zhf_source.did_name)
ttbar_sample_naming = SampleNaming(ttbar_source.did_name)
whf_taunu_sample_naming = SampleNaming(whf_taunu_source.did_name)
whf_enu_sample_naming = SampleNaming(whf_enu_source.did_name)
whf_munu_sample_naming = SampleNaming(whf_munu_source.did_name)
jz3w_sample_naming = SampleNaming(jz3w_source.did_name)
jz4w_sample_naming = SampleNaming(jz4w_source.did_name)
jz5w_sample_naming = SampleNaming(jz5w_source.did_name)
jz6w_sample_naming = SampleNaming(jz6w_source.did_name)
data_sample_naming = SampleNaming(data_source.did_name)

sample_naming_dict = { 'X1000' : sig10_sample_naming,
              'X1600' : sig16_sample_naming,
              'X2000' : sig20_sample_naming,
              'Z+jets' : zhf_sample_naming,
              'ttbar' : ttbar_sample_naming,
              'W+jets_taunu' : whf_taunu_sample_naming,
              'W+jets_enu' : whf_enu_sample_naming,
              'W+jets_munu' : whf_munu_sample_naming,
              'JZ3W' : jz3w_sample_naming,
              'JZ4W' : jz4w_sample_naming,
              'JZ5W' : jz5w_sample_naming,
              'JZ6W' : jz6w_sample_naming,
              'Data' : data_sample_naming,
              }
